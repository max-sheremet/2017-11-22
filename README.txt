2017-11-22 - WILDFLY first project from [http://www.thejavageek.com/2015/12/16/jax-rs-hello-world-example-with-wildfly/]
1. mvn archetype:generate -DgroupId=com.sheremet -DartifactId=HelloWorldEJB -D-DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false
2. [/opt/wildfly/standalone/standalone-full.xml + /opt/wildfly/standalone/standalone-full.xml]
3. in [pom.xml]
-change packaging to WAR
-add
    <dependency>
      <groupId>javax.ws.rs</groupId>
      <artifactId>javax.ws.rs-api</artifactId>
      <version>2.0</version>
    </dependency>
-add
  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-war-plugin</artifactId>
        <version>2.2</version>
        <configuration>
          <failOnMissingWebXml>false</failOnMissingWebXml>
        </configuration>
      </plugin>
    </plugins>
    <!-- we dont want the version to be part of the generated war file name -->
    <finalName>${project.artifactId}</finalName>
  </build>

4. run [sudo ./add-user.sh] in [/opt/wildfly/bin]
-a,user,password,yes,password,blank,yes,yes
5. restart WF
6. goto {IP:8080] and click [Administration Console] >> login >> deployment >> add the WAR from target
7. goto [IP:8080/HelloWorldEJB/rest/HelloWorld/sayHello]
