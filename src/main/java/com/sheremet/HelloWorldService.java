package com.sheremet;

import com.sheremet.greeting.GreetingServiceImplEJB;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/HelloWorld")
public class HelloWorldService {

    @Resource(mappedName = "greetingService", lookup = "java:app/com/sheremet/greeting/GreetingServiceImplEJB!com.sheremet.greeting.GreetingService")
    private GreetingServiceImplEJB greetingServiceImpl;

    @GET
    @Path("/sayHello")
    public String sayHello() {
        return String.format("<h2>%s</h2>", greetingServiceImpl.saySomething());
    }

}