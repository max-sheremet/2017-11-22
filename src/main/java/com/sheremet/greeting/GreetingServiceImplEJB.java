package com.sheremet.greeting;

import java.util.UUID;

/**
 * Created by max on 23/11/17.
 */
public class GreetingServiceImplEJB implements GreetingService {
    public String saySomething() {
        return String.format("Hello[%s]", UUID.randomUUID().toString());
    }
}
